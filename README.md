[![coverage report](https://gitlab.com/timmw/nonzo/badges/master/coverage.svg)](https://gitlab.com/timmw/nonzo/commits/master)

# Nonzo

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Run your tests

```
yarn run test
```

### Lints and fixes files

```
yarn run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

import { createRouter, createWebHistory } from "vue-router";
import HomeView from "./views/HomeView.vue";
import AuthView from "./views/AuthView.vue";
import AuthConfirmView from "./views/AuthConfirmView.vue";
import MonthlySpend from "./views/MonthlySpend.vue";

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/auth",
      name: "auth",
      component: AuthView,
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      // component: () =>
      //   import("./views/About.vue")
    },
    {
      path: "/auth-confirm",
      name: "auth-confirm",
      component: AuthConfirmView,
    },
    {
      path: "/monthly-spend",
      name: "monthly-spend",
      component: MonthlySpend,
    },
  ],
});

import { createApp } from "vue";
import VueLazyload from "vue-lazyload";
import App from "./App.vue";
import { router } from "./router";
import { createStore } from "./store/index.js";
import { MonzoApiClient } from "@/lib/monzo-api";
import VueVirtualScroller from "vue-virtual-scroller";
import "vue-virtual-scroller/dist/vue-virtual-scroller.css";
import { transactions } from "@/lib/test-data";
import { db } from "@/db.js";

const init = async () => {
  const monzoApiClient = new MonzoApiClient(
    // @ts-ignore
    window.location.origin + import.meta.env.BASE_URL + "auth-confirm/"
  );

  const store = createStore(monzoApiClient);

  if (process.env.NODE_ENV === "development") {
    // @ts-ignore
    window.store = store;
    // @ts-ignore
    window.router = router;
    // @ts-ignore
    window.db = db;
    // @ts-ignore
    window.api = monzoApiClient;

    window.reset = () => {
      localStorage.clear();
      db.delete();
    };
  }

  if ((await db.transactions.count()) === 0) {
    // popuate db with dummy transactions
    await db.transactions.bulkPut(transactions(1000));
  }

  const app = createApp({
    ...App,
  });

  app.use(VueVirtualScroller);
  app.use(VueLazyload);
  app.use(store);
  app.use(router);

  app.provide("db", db);
  app.provide("monzoApiClient", monzoApiClient);

  app.mount("#app");
};

init();

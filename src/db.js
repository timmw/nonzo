import Dexie from "dexie";

export const db = new Dexie("nonzo");

db.version(2).stores({
  accounts: "id",
  transactions: "id,amount,created,category,account_id",
});

db.open();

export const ACCESS_TOKEN = "accessToken";
export const ACCESS_TOKEN_CREATED_AT = "accessTokenCreatedAt";

// Monzo CSRF secret https://docs.monzo.com/#acquire-an-access-token
export const STATE = "state";
export const TRANSACTIONS = "transactions";
export const HIDDEN_TRANSACTIONS = "hiddenTransactions";

/**
 * Functions for dealing with transactions
 */
import * as R from "ramda";
import * as RA from "ramda-adjunct";
import { format, parseJSON } from "date-fns/fp";
import { isThisYear } from "date-fns";

/** @typedef {Object} Transaction */

const gbpFormat = new Intl.NumberFormat("en-GB", {
  style: "decimal",
  minimumFractionDigits: 2,
});

export const formatAmount = (amount) => gbpFormat.format(amount / 100);

/**
 * @param {Array} txs
 */
export const groupByDate = (txs) =>
  R.groupBy(
    (tx) =>
      format(
        "iiii do MMMM" + (!isThisYear(new Date(tx.created)) ? " y" : ""),
        new Date(tx.created)
      ),
    txs
  );

export const groupByMonth = (txs) =>
  R.groupBy(R.compose(format("yyyy-MM"), parseJSON, R.prop("created")), txs);

export const sortNewestFirst = (txs) =>
  R.sort(R.descend(R.prop("created")), txs);

export const isInScope = (tx) => tx.category === "bills" && tx.amount < 0;

/**
 * {Transaction[]} => {String[]}
 */
export const uniqueCategories = (txs) =>
  R.compose(R.sortBy(R.identity), R.uniq, R.pluck("category"))(txs);

export const totalAmount = (txs) => R.compose(R.sum, R.pluck("amount"))(txs);

/** Whether the transaction is a contribution towards existing transaction (bill split) */
export const isContribution = (tx) =>
  !!tx.metadata.original_transaction_id || tx.metadata.$contributionTo;

export const withContributions = (txs, contribs) =>
  R.map((tx) => R.assoc("$contributions", contribs[tx.id] || [], tx), txs);

export const contributions = (txs) =>
  R.reduce(
    (acc, tx) => {
      let originalTxId = tx.metadata.original_transaction_id;

      if (tx.metadata.$contributionTo) {
        originalTxId = tx.metadata.$contributionTo[0];
      }

      if (originalTxId !== undefined) {
        return R.assoc(
          originalTxId,
          R.append(tx, acc[originalTxId] || []),
          acc
        );
      }

      return acc;
    },
    {},
    txs
  );

export const filteredTransactions = (filters, txs, hiddenTransactionIds) =>
  R.filter((tx) => {
    const matchesCategory = filters.selectedCategories.length
      ? filters.selectedCategories.includes(tx.category)
      : true;
    const matchesProperties = filters.property.name
      ? tx[filters.property.name] == filters.property.value
      : true;
    return (
      matchesCategory &&
      !isContribution(tx) &&
      !["SALARY", "ENTS 24 LIMITED"].includes(tx.description) &&
      hiddenTransactionIds[tx.id] !== true &&
      matchesProperties
    );
  }, txs);

export const properties = (txs) =>
  R.reduce(
    (acc, tx) => {
      const props = getProperties(tx);

      R.forEachObjIndexed((type, key) => {
        acc[key] = acc[key] || new Set();
        acc[key].add(type);
      }, props);
      return acc;
    },
    {},
    txs
  );

/** @param Transaction */
export const getProperties = (obj) =>
  R.reduce(
    (acc, [key, val]) => {
      acc[key] = R.type(val);
      if (acc[key] === "Object") {
        acc = {
          ...acc,
          ...RA.renameKeysWith(R.concat(`${key}.`), getProperties(val)),
        };
      }
      return acc;
    },
    {},
    R.toPairs(obj)
  );

export const isPotTx = (tx) =>
  tx.description && tx.description.indexOf("pot_") === 0;

export const title = (tx) =>
  isPotTx(tx)
    ? "Pot payment"
    : tx?.merchant?.name ?? tx?.counterparty?.name ?? tx.description;

const customMetadataKeys = ["$contributionTo"];

export const parseMetadata = (tx) => {
  const metadata = R.mapObjIndexed(
    (value, key) =>
      customMetadataKeys.includes(key) ? JSON.parse(value) : value,
    tx.metadata
  );

  return R.assoc("metadata", metadata, tx);
};

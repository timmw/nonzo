import categories from "./data/categories.json";
import companies from "./data/companies.json";

import { formatRFC3339, parseISO, addHours } from "date-fns/fp";

/**
 * @return Number min <= return < max
 */
const randomInt = (min, max) => Math.floor(Math.random() * (max - min)) + min;

function randomDate(since = null) {
  since = since || new Date(2019, 0);
  return formatRFC3339(randomInt(since.getTime(), Date.now()));
}

const randomCategory = () => categories[randomInt(0, categories.length)];

const randomCompany = () => companies[randomInt(0, companies.length)];

const idGenerator = (prefix = "") => {
  idGenerator[prefix] = idGenerator[prefix] || 1;
  return prefix + idGenerator[prefix]++;
};

const counterparty = {
  name: "Mr Moneybags",
};

function dummyContribution({ category, amount, id, created }) {
  return {
    ...dummyTransaction(),
    merchant: undefined,
    created: formatRFC3339(addHours(randomInt(1, 37), parseISO(created))),
    counterparty,
    category,
    amount: Math.floor(Math.abs(amount) / 2),
    metadata: {
      original_transaction_id: id,
    },
  };
}

function dummyTransaction() {
  const category = randomCategory();

  return {
    id: idGenerator("tx_"),
    created: randomDate(),
    description: "",
    amount: randomInt(-5000, -100),
    merchant: {
      name: randomCompany(),
      logo: "",
      category,
    },
    metadata: {},
    category,
    include_in_spending: true,
    can_be_excluded_from_breakdown: true,
    can_be_made_subscription: true,
    can_split_the_bill: true,
    can_add_to_tab: true,
    amount_is_pending: false,
  };
}

export function transactions(n = 100) {
  const txs = [];

  for (let i = 0; i < n; i++) {
    txs.push(dummyTransaction());
  }

  const contributions = [];

  for (const tx of txs) {
    if (Math.random() < 0.15) {
      contributions.push(dummyContribution(tx));
    }
  }

  return txs.concat(contributions);
}

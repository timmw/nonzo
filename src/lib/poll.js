/**
 * From https://davidwalsh.name/javascript-polling
 * @param {Function} fn
 * @param {Number} timeout in ms
 * @param {Number} interval in ms
 * @returns {Promise<unknown>}
 */
export function poll(fn, timeout, interval) {
  const endTime = Date.now() + timeout;

  const checkCondition = async function (resolve, reject) {
    // If the condition is met, we're done!
    const result = await fn();
    if (result) {
      resolve(result);
    }
    // If the condition isn't met but the timeout hasn't elapsed, go again
    else if (Date.now() < endTime) {
      setTimeout(checkCondition, interval, resolve, reject);
    }
    // Didn't match and too much time, reject!
    else {
      reject(new Error("timed out for " + fn + ": " + arguments));
    }
  };

  return new Promise(checkCondition);
}

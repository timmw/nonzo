import * as t from "./transaction";

const FIXED_SYSTEM_TIME = "2020-01-01T01:00:00Z";

describe("transaction", () => {
  beforeEach(() => {
    jest.useFakeTimers("modern");
    jest.setSystemTime(Date.parse(FIXED_SYSTEM_TIME));
  });

  afterEach(() => {
    jest.useRealTimers();
  });

  test("getProperties", () => {
    expect(t.getProperties({ outer: { inner: "value" } })).toEqual({
      outer: "Object",
      "outer.inner": "String",
    });
  });

  test("properties", () => {
    const list = [{ prop: "value" }, { prop: 1 }];
    expect(t.properties(list)).toEqual({
      prop: new Set(["String", "Number"]),
    });
  });

  test("formatAmount", () => {
    expect(t.formatAmount(1000000)).toEqual("10,000.00");
  });

  test("groupByDate returns object with correct keys", () => {
    expect(
      Object.keys(
        t.groupByDate([
          { created: "2020-01-01" },
          { created: "2020-01-01" },
          { created: "2020-01-02" },
          { created: "2021-01-01" },
        ])
      )
    ).toEqual([
      "Wednesday 1st January",
      "Thursday 2nd January",
      "Friday 1st January 2021",
    ]);
  });

  test("groupByMonth", () => {
    expect(
      Object.keys(
        t.groupByMonth([
          { created: "2020-01-01T00:00:08.000Z" },
          { created: "2020-01-02T00:00:08.000Z" },
          { created: "2021-02-01T00:00:08.000Z" },
        ])
      )
    ).toEqual(["2020-01", "2021-02"]);
  });

  test("sortNewestFirst", () => {
    expect(
      t.sortNewestFirst([
        { created: "2020-01-01T00:00:08.000Z" },
        { created: "2020-01-02T00:00:08.000Z" },
      ])
    ).toEqual([
      { created: "2020-01-02T00:00:08.000Z" },
      { created: "2020-01-01T00:00:08.000Z" },
    ]);
  });

  test("parseMetadata with no custom properties", () => {
    expect(
      t.parseMetadata({
        metadata: { someParam: "someValue" },
      })
    ).toEqual({
      metadata: { someParam: "someValue" },
    });
  });

  test("parseMetadata with custom $contributionTo property", () => {
    expect(
      t.parseMetadata({
        metadata: { someParam: "someValue", $contributionTo: `["tx_1"]` },
      })
    ).toEqual({
      metadata: { someParam: "someValue", $contributionTo: ["tx_1"] },
    });
  });

  test("title for pot payment", () => {
    expect(
      t.title({
        description: "pot_1",
      })
    ).toEqual("Pot payment");
  });

  test("title for merchant transaction", () => {
    expect(
      t.title({
        merchant: {
          name: "ACME Corp.",
        },
      })
    ).toEqual("ACME Corp.");
  });

  test("title for payment to/from person", () => {
    expect(
      t.title({
        counterparty: {
          name: "Wile E. Coyote",
        },
      })
    ).toEqual("Wile E. Coyote");
  });

  test("title for other payment", () => {
    expect(
      t.title({
        description: "Some other payment",
      })
    ).toEqual("Some other payment");
  });
});

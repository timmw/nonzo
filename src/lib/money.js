const gbpFormat = new Intl.NumberFormat("en-GB", {
  style: "decimal",
  minimumFractionDigits: 2,
});

export function formatAmount(amount) {
  return gbpFormat.format(amount / 100);
}

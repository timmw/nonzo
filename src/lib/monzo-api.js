import { format, sub } from "date-fns/fp";
import ky from "ky";
import { v4 as uuidv4 } from "uuid";

const API_BASE_URL = "https://api.monzo.com";

const RFC3339_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

const LS_KEY = "monzoApiClientState";

class LocalStorageStateStore {
  #lsKey = null;
  #state = {};

  constructor(key) {
    this.#lsKey = key;
    this.#load();

    window.addEventListener("storage", (event) => {
      if (event.key === this.#lsKey) {
        this.#load();
      }
    });
  }

  getItem(key) {
    return this.#state[key];
  }

  set(state) {
    this.#state = {
      ...this.#state,
      ...state,
    };
    this.#save();
  }

  setItem(key, val) {
    this.set({ [key]: val });
  }

  clear() {
    this.#state = {};
    localStorage.removeItem(this.#lsKey);
  }

  #save() {
    localStorage.setItem(this.#lsKey, JSON.stringify(this.#state));
  }

  #load() {
    this.#state = JSON.parse(localStorage.getItem(this.#lsKey) ?? "{}");
  }
}

export class TokenEvictedError extends Error {}

export class MonzoApiClient {
  #client;
  #storage;
  #redirectUri;
  #logger;

  constructor(redirectUri, storage = null, requestLogger = null) {
    this.#redirectUri = redirectUri;
    this.#storage = storage ?? new LocalStorageStateStore(LS_KEY);
    this.#logger =
      requestLogger ??
      (({ request, options, response }) => {
        // const path = new URL(request.url).pathname;
        console.debug({ request, options, response });
      });
    this.#storage.setItem("oauthState", this.oauthState ?? uuidv4());

    this.#client = ky.create({
      prefixUrl: API_BASE_URL,
      hooks: {
        afterResponse: [
          (options) => this.#logger(options),
          (request, options, response) =>
            this.#refreshTokenAndRetry(request, options, response),
        ],
        beforeRequest: [
          (request) => {
            const token = this.#storage.getItem("accessToken");
            if (token) {
              request.headers.set("Authorization", `Bearer ${token}`);
            }
          },
        ],
      },
      timeout: 30000,
    });
  }

  async #refreshTokenAndRetry(request, options, response) {
    if (response?.status === 401) {
      const { accessToken } = await this.refreshAccessToken();
      request.headers.set("Authorization", `Bearer ${accessToken}`);

      return await ky(request);
    }

    return response;
  }

  get clientId() {
    return this.#storage.getItem("clientId");
  }

  set clientId(id) {
    this.#storage.setItem("clientId", id);
  }

  get clientSecret() {
    return this.#storage.getItem("clientSecret");
  }

  set clientSecret(secret) {
    this.#storage.setItem("clientSecret", secret);
  }

  get oauthState() {
    return this.#storage.getItem("oauthState");
  }

  clear() {
    this.#storage.clear();
  }

  async isAuthenticated() {
    const response = await fetch(`${API_BASE_URL}/ping/whoami`, {
      headers: {
        Authorization: `Bearer ${this.#storage.getItem("accessToken")}`,
        "Content-Type": "application/json",
      },
    });

    const body = await response.json();

    if (response.status === 401) {
      switch (body.code) {
        case "unauthorized.bad_access_token.evicted":
          throw new TokenEvictedError(body.message);
      }
    }

    return body.authenticated === true;
  }

  async getAccessToken({ grantType = "authorization_code", code = null }) {
    const params = new URLSearchParams({
      grant_type: grantType,
      client_id: this.clientId,
      client_secret: this.clientSecret,
      redirect_uri: this.#redirectUri,
    });

    switch (grantType) {
      case "authorization_code":
        if (code === null) {
          throw Error(
            "code must be provided if grantType is authorization_code"
          );
        }
        params.set("code", code);
        break;
      case "refresh_token":
        params.set("refresh_token", this.#storage.getItem("refreshToken"));
        break;
    }

    /**
     * @type {Object}
     */
    const response = await ky
      .post(`${API_BASE_URL}/oauth2/token`, {
        body: params,
      })
      .json();

    this.#storage.set({
      accessToken: response.access_token,
      refreshToken: response.refresh_token,
    });

    return {
      accessToken: response.access_token,
      refreshToken: response.refresh_token,
      createdAt: Date.now(),
      expiresIn: response.expires_in,
      userId: response.user_id,
    };
  }

  async refreshAccessToken() {
    return this.getAccessToken({ grantType: "refresh_token" });
  }

  async getAccounts() {
    /** @type {Object} */
    const response = await this.#client
      .get("accounts", {
        searchParams: {
          account_type: "uk_retail",
        },
      })
      .json();

    // todo: handle possibility of 403 response due to
    // forbidden.insufficient_permissions (user hasn't approved access via their app)

    return response.accounts;
  }

  async getTransactions(accountId, since = null) {
    const _since = format(
      RFC3339_FORMAT,
      since ?? sub({ days: 89 }, Date.now())
    );
    /** @type {Object} */
    const response = await this.#client
      .get("transactions", {
        searchParams: {
          account_id: accountId,
          since: _since,
          "expand[]": "merchant",
        },
      })
      .json();

    return response.transactions;
  }

  /**
   * @param {String} txId
   * @param {Object} metadata
   */
  async patchTransaction(txId, metadata) {
    const params = new URLSearchParams();

    for (let key of Object.keys(metadata)) {
      params.set(`metadata[${key}]`, metadata[key]);
    }

    const response = await this.#client
      .patch(`transactions/${txId}`, {
        body: params.toString(),
        hooks: {
          beforeRequest: [
            (request) => {
              request.headers.set(
                "Content-Type",
                "application/x-www-form-urlencoded"
              );
            },
          ],
        },
      })
      .json();

    return response;
  }

  authUrl() {
    const searchParams = new URLSearchParams({
      client_id: this.clientId,
      redirect_uri: this.#redirectUri,
      state: this.oauthState,
      response_type: "code",
    });

    return "https://auth.monzo.com/?" + searchParams.toString();
  }
}

import * as AUTH_STATUS from "@/lib/auth-status";
import * as R from "ramda";

const defaultState = Object.freeze({
  status: AUTH_STATUS.UNAUTHENTICATED,
  accessTokenExpiresIn: null,
  accessTokenCreatedAt: null,
  accessToken: null,
  refreshToken: null,
});

export default (monzoApiClient) => ({
  namespaced: true,
  state: R.clone(defaultState),
  mutations: {
    SET_AUTH_STATUS(state, status) {
      state.status = status;
    },
    SET_ACCESS_TOKEN(
      state,
      { expiresIn, createdAt, accessToken, refreshToken }
    ) {
      state.accessTokenExpiresIn = expiresIn;
      state.accessTokenCreatedAt = createdAt;
      state.accessToken = accessToken;
      state.refreshToken = refreshToken;
    },
    RESET(state) {
      Object.assign(state, R.clone(defaultState));
    },
  },
  actions: {
    async LOG_IN({ commit }, code) {
      commit("SET_ACCESS_TOKEN", await monzoApiClient.getAccessToken({ code }));
      commit("SET_AUTH_STATUS", AUTH_STATUS.PENDING_APP_APPROVAL);
    },
    async CHECK_AUTH_STATUS({ commit }) {
      commit(
        "SET_AUTH_STATUS",
        (await monzoApiClient.isAuthenticated())
          ? AUTH_STATUS.AUTHENTICATED
          : AUTH_STATUS.UNAUTHENTICATED
      );
    },
  },
  getters: {
    accessTokenExpiresAt(state) {
      return state.accessTokenCreatedAt + state.accessTokenExpiresIn;
    },
    authenticated(state) {
      return state.status === AUTH_STATUS.AUTHENTICATED;
    },
  },
});

import { createStore as vuexCreateStore } from "vuex";
import VuexPersistence from "vuex-persist";
import auth from "./auth";
import filters from "./filters";
import transactions from "./transactions";

// This is the date my residual balance was
// transferred from prepaid card into current account
const ACCOUNT_OPENED_ON = "2017-11-02";

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  reducer: ({ tx, accounts, auth, userDefinedFilters }) => ({
    accounts,
    auth: {
      accessTokenCreatedAt: auth.accessTokenCreatedAt,
      accessTokenExpiresIn: auth.accessTokenExpiresIn,
    },
    tx: {
      transactions: tx.transactions,
      lastTxRequestTimestamp: tx.lastTxRequestTimestamp,
      categories: tx.categories,
      hiddenTransactionIds: tx.hiddenTransactionIds,
    },
    userDefinedFilters,
  }),
});

const MODE_DEMO = "tx-demo";
const MODE_REAL = "tx";

export const createStore = (monzoApiClient) =>
  vuexCreateStore({
    strict: process.env.NODE_ENV !== "production",
    plugins: [vuexLocal.plugin],
    modules: {
      filters,
      [MODE_REAL]: transactions(monzoApiClient),
      [MODE_DEMO]: transactions(monzoApiClient),
    },
    state: {
      userDefinedFilters: [],
      mode: MODE_DEMO,
    },
    mutations: {
      SET_MODE_DEMO(state) {
        state.mode = MODE_DEMO;
      },
      SET_MODE_REAL(state) {
        state.mode = MODE_REAL;
      },
    },
    actions: {
      LOGOUT({ commit }) {
        commit("RESET");
      },
    },
    getters: {
      isDemoMode(state) {
        return state.mode === MODE_DEMO;
      },
    },
  });

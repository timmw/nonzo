import * as R from "ramda";

export default {
  namespaced: true,
  state: {
    selectedCategories: [],
    property: {},
  },
  mutations: {
    SET(state, filters) {
      Object.assign(state, filters);
    },
  },
  actions: {},
  getters: {},
};

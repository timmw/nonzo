import * as R from "ramda";
import * as t from "@/lib/transaction";

const mutations = {
  SORT_TRANSACTIONS(state) {
    state.transactions = t.sortNewestFirst(state.transactions);
  },
  SET_LAST_TX_REQUEST_TIMESTAMP(state, timestamp) {
    state.lastTxRequestTimestamp = timestamp;
  },
  UPDATE_CATEGORIES(state) {
    state.categories = t.uniqueCategories(state.transactions);
  },
  UPDATE_TRANSACTION(state, tx) {
    const index = R.findIndex((_tx) => _tx.id === tx.id, state.transactions);

    if (index !== -1) {
      state.transactions[index] = tx;
    }
  },
};

export const actions = (monzoApiClient) => ({
  SET_TRANSACTIONS({ commit }, txs) {
    commit("UPDATE_CATEGORIES");
  },
});

export const getters = {
  contributions(state) {
    return t.contributions(state.transactions);
  },
  filteredTransactions(
    { transactions: txs, hiddenTransactionIds },
    _getters,
    { filters }
  ) {
    return t.filteredTransactions(filters, txs, hiddenTransactionIds);
  },
  balance(_state, getters) {
    return t.totalAmount(getters.filteredTransactions);
  },
  filteredTransactionsWithContributions(_state, getters) {
    return t.withContributions(
      getters.filteredTransactions,
      getters.contributions
    );
  },
  properties(state) {
    return t.properties(state.transactions);
  },
};

export default (monzoApiClient) => ({
  namespaced: true,
  state: () => ({
    categories: [],
  }),
  mutations,
  actions: actions(monzoApiClient),
  getters,
});
